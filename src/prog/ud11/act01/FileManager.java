package prog.ud11.act01;

import java.io.File;
import java.io.IOException;

public class FileManager {
    public static void crearFichero(String ruta, String nombre) throws IOException {
        File f = new File(ruta, nombre);
        if (!f.exists()) {
            f.createNewFile();
        }
    }    
    
    public static void verArchivosDirectorio(String directorio) {
        File f = new File(directorio);
        if (f.isDirectory()) {
            String[] lista = f.list();
            for (String archivo : lista) {
                System.out.println(archivo);
            }
        }
            
    }
    
    public static void verInfoArchivo(String ruta, String nombre) {
        File f = new File(ruta, nombre);
        System.out.println("Nombre: " + f.getName());
        System.out.println("Permiso escritura: " + f.canWrite());
        System.out.println("Permiso lectura: " + f.canRead());
        System.out.println("Ruta absoluta: " + f.getAbsolutePath());
        System.out.println(f.isFile()? "Es archivo": "Es directorio");
    }
}
