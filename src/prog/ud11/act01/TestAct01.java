package prog.ud11.act01;

import java.io.IOException;

public class TestAct01 {
    public static void main(String[] args) throws IOException {
        String rutaHome = System.getProperty("user.home");
        
        FileManager.crearFichero(rutaHome, "adios.txt");
        
        FileManager.verArchivosDirectorio(rutaHome);
        
        FileManager.verInfoArchivo(rutaHome, "adios.txt");
    }
}
