package prog.ud11;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class act04 {

    public static void main(String[] args) {
        File f = new File("resources", "productos.txt");
        int numTeles = 0, numMonitor = 0, numTeclat = 0;

        System.out.println("Leyendo productos...");
        try (BufferedReader br = new BufferedReader(new FileReader(f));) 
        {
            String lin;
            while ((lin = br.readLine()) != null) {
                String[] valores = lin.split(",");
                switch (valores[1]) {
                    case "Televisió" -> numTeles++;
                    case "Monitor" -> numMonitor++;
                    case "Teclat" -> numTeclat++;                    
                }
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Fichero no existe " + ex.getMessage());
        } catch (IOException ioex) {
            System.out.println("Problemas e/s" + ioex.getMessage());
        }
        
        System.out.println("Num televisores: " + numTeles);
        System.out.println("Num monitores: " + numMonitor);
        System.out.println("Num teclados: " + numTeclat);

    }
}
