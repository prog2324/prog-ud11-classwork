package prog.ud11;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class act05 {

    public static void main(String[] args) {
        Path p1 = Paths.get("resources", "moduls1.txt");
        Path p2 = Paths.get("resources", "moduls2.txt");
        //BufferedReader br = Files.newBufferedReader(p);

        System.out.println("Copiando fichero y transformando a mayúsculas...");
        try (BufferedReader br = new BufferedReader(new FileReader(p1.toFile())); 
             BufferedWriter bw = new BufferedWriter(new FileWriter(p2.toFile()));) 
        {
            String lin, linMayus;
            while ((lin = br.readLine()) != null) {
                linMayus = lin.toUpperCase();
                bw.write(linMayus);
                bw.newLine();
                System.out.println("Linea copiada!");
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Fichero no existe " + ex.getMessage());
        } catch (IOException ioex) {
            System.out.println("Problemas e/s" + ioex.getMessage());
        }
        
        System.out.println("Fin proceso de copiado");

    }
}
