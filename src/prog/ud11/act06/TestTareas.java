package prog.ud11.act06;

import java.io.IOException;
import java.util.List;

// Faltaria hacer: autonumérico
// O evitar crear tareas cuyo id ya existe.
public class TestTareas {

    public static void main(String[] args) {

        try {
            FileTareaDAO tareaFile = new FileTareaDAO();

            Tarea t1 = new Tarea(1, "Instal·lar sistema", "Roberto", false);
            Tarea t2 = new Tarea(2, "Formatejar disc", "Sergio", false);
            Tarea t3 = new Tarea(3, "Programar exercici", "Raul", true);
            Tarea t4 = new Tarea(4, "Respirar", "Sergio", false);
            Tarea t5 = new Tarea(5, "Hacer deporte", "Sergio", true);
            Tarea t6 = new Tarea(6, "Comer", "Roberto", false);

            tareaFile.add(t1);
            System.out.println("Tarea " + t1.getCodigo() + " añadida con éxito");
            tareaFile.add(t2);
            System.out.println("Tarea " + t2.getCodigo() + " añadida con éxito");
            tareaFile.add(t3);
            System.out.println("Tarea " + t3.getCodigo() + " añadida con éxito");
            tareaFile.add(t4);
            System.out.println("Tarea " + t4.getCodigo() + " añadida con éxito");
            tareaFile.add(t5);
            System.out.println("Tarea " + t5.getCodigo() + " añadida con éxito");
            tareaFile.add(t6);
            System.out.println("Tarea " + t6.getCodigo() + " añadida con éxito");
            System.out.println("----------------");

            System.out.println("Todas las tareas");
            List<Tarea> tareas = tareaFile.findAll();
            mostrarTareas(tareas);
            System.out.println("----------------");

            System.out.println("Tareas de Sergio");
            tareas = tareaFile.findByUser("Sergio");
            mostrarTareas(tareas);
            System.out.println("----------------");

            System.out.println("Tarea código 2");
            Tarea t = tareaFile.findByCod(2);
            System.out.println(t);
            System.out.println("----------------");

            System.out.println("Actualizando tarea " + t4);
            t4.setDescripcion("Modificada");
            t4.setUsuario("Roberto");
            t4.setRealizada(true);
            tareaFile.update(t4);
            tareas = tareaFile.findAll();
            mostrarTareas(tareas);
            System.out.println("----------------");

            System.out.println("Borrando tarea " + t5);
            tareaFile.delete(t5);
            tareas = tareaFile.findAll();
            mostrarTareas(tareas);
            System.out.println("----------------");

        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }

    }

    private static void mostrarTareas(List<Tarea> tareas) {
        System.out.print((tareas.isEmpty()) ? "Sin tareas\n" : "");
        for (Tarea t : tareas) {
            System.out.println(t);
        }
    }
}
