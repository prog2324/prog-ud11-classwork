package prog.ud11.act06;

import java.util.Objects;

public class Tarea {

    private int codigo;
    private String descripcion;
    private String usuario;
    private boolean realizada;

    public Tarea(int codigo, String descripcion, String usuario, boolean realizada) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.usuario = usuario;
        this.realizada = realizada;
    }

    public void setCodigo(int cod) {
        this.codigo = cod;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    public String getDescripcion() {
        return descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean isRealizada() {
        return realizada;
    }

    public void setRealizada(boolean realizada) {
        this.realizada = realizada;
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Tarea other = (Tarea) obj;
        return codigo == other.codigo;
    }

    @Override
    public String toString() {
        return "Tarea [codigo=" + codigo + ", usuario=" + usuario + ", descripcion=" + descripcion + " (" + (realizada? "SI" : "NO") + " realizada)]";
    }
    
    public String toStringFile() {
        return codigo + ";" + descripcion + ";" + usuario + ";" + (realizada? "SI": "NO");
    }

}
