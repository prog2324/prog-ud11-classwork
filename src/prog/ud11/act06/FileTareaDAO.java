package prog.ud11.act06;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileTareaDAO {

    private final static String NOM_FICHERO = "tareas.txt";
    private final File fichero;

    public FileTareaDAO() throws IOException {
        fichero = new File(NOM_FICHERO);
        
//        if (fichero.exists()) { fichero.delete(); }
        
        // No necesario:
        if (!fichero.exists()) {
            fichero.createNewFile();
        }
    }

    public Tarea findByCod(int cod) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(fichero))) {
            String linea;
            Tarea tarea;
            while ((linea = br.readLine()) != null) {
                String[] valores = linea.split(";");
                int codigo = Integer.parseInt(valores[0]);
                if (codigo == cod) {
                    tarea = new Tarea(codigo, valores[1], valores[2], ("SI".equals(valores[3])));
                    return tarea;
                }
            }
        }
        return null;
    }

    public List<Tarea> findAll() throws IOException {
        return findBy(0, ".*");
    }

    public List<Tarea> findByUser(String usuario) throws IOException {
        return findBy(2, usuario);
    }

    private List<Tarea> findBy(int campo, String valor) throws IOException {
        List<Tarea> lista = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(fichero))) {
            String linea;
            Tarea tarea;
            while ((linea = br.readLine()) != null) {
                String[] valores = linea.split(";");
                if (valores[campo].matches(valor)) {
                    tarea = new Tarea(Integer.parseInt(valores[0]), valores[1], valores[2], ("SI".equals(valores[3])));
                    lista.add(tarea);
                }
            }
        }
        return lista;
    }

    public boolean add(Tarea tarea) throws IOException {
        
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fichero, true))) {
            if (fichero.length() > 0) {
                bw.newLine();
            }
            bw.write(tarea.toStringFile());
            return true;
        }
    }

    public void delete(Tarea tarea) throws IOException {
        List<Tarea> tareas = findAll();
        tareas.remove(tarea);
        guardarEnFichero(tareas);
    }

    public void update(Tarea tarea) throws IOException {
        List<Tarea> tareas = findAll();
        int i = tareas.indexOf(tarea);
        Tarea t = tareas.get(i);
        t.setDescripcion(tarea.getDescripcion());
        t.setUsuario(tarea.getUsuario());
        t.setRealizada(tarea.isRealizada());
        guardarEnFichero(tareas);
    }

    private void guardarEnFichero(List<Tarea> tareas) throws IOException {
        
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fichero, false))) {
            int i;
            for (i = 0; i < tareas.size() - 1; i++) {
                bw.write(tareas.get(i).toStringFile());
                bw.newLine();
            }
            bw.write(tareas.get(i).toStringFile());
        }
    }
}
